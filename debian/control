Source: chemeq
Section: tex
Priority: optional
Maintainer: Georges Khaznadar <georgesk@debian.org>
Build-Depends: debhelper-compat (=13),
 flex,
 bison,
 libfl-dev,
 python3-bs4,
 python3-jinja2,
 python3-lxml,
 python3-cssmin,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/chemeq
Vcs-Git: https://salsa.debian.org/science-team/chemeq.git

Package: chemeq
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: texlive, wims
Description: Parser for chemical formula and equilibria
 chemeq is a basic standalone filter written in C language,
 flex and bison. It inputs strings like:
  2H2 + O2 ---> 2 H2O
 then it outputs LaTeX code and messages about the equilibrium of a
 chemical reaction.
 .
  example:~/src$ echo "2H2 + O2 ---> 2 H2O" | chemeq -lc
  2\,H_{2}\,+\,O_{2}\,\rightarrow\,2\,H_{2}O
  OK
